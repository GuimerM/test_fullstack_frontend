<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Amenity;

class AmenitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {

        $amenities =  array(
            [
                'name' => 'Alberca',
            ],
            [
                'name' => 'Amueblado',
            ],
            [
                'name' => 'Área de lavado',
            ],
            [
                'name' => 'Cisterna',
            ],
            [
                'name' => 'Seguridad privada',
            ],
            [
                'name' => 'Terraza',
            ],
            [
                'name' => 'Cuarto de servicio',
            ],
            [
                'name' => 'Jardín',
            ],
            [
                'name' => 'Estacionamiento',
            ],
            [
                'name' => 'Roof garden',
            ]
        );

        foreach ($amenities as $item){
            $amenity = new Amenity();
            $amenity->name =$item['name'];
            $amenity->save();
        }

    }
}
