<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('logout', 'App\Http\Controllers\AuthController@logout');
    Route::post('refresh', 'App\Http\Controllers\AuthController@refresh');
    Route::get('current-user', 'App\Http\Controllers\AuthController@getAuthenticatedUser');
    Route::post('register', 'App\Http\Controllers\AuthController@register');

    Route::post('properties/', 'App\Http\Controllers\PropertyController@store');
    Route::put('properties/{property}', 'App\Http\Controllers\PropertyController@update');
    Route::delete('properties/{property}', 'App\Http\Controllers\PropertyController@delete');
    Route::delete('image/{image}', 'App\Http\Controllers\PropertyController@deleteFile');
});

Route::get('properties', 'App\Http\Controllers\PropertyController@index');
Route::get('properties/{property}', 'App\Http\Controllers\PropertyController@show');
Route::get('amenities', 'App\Http\Controllers\AmenityController@index');
Route::put('sproperties/{property}', 'App\Http\Controllers\PropertyController@testFile');