<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Amenity;

class AmenityController extends Controller
{
    public function index(){
        $amenities = Amenity::all();

        return $amenities;
    }
}
