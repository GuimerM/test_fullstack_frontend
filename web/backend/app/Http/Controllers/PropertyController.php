<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\PropertyAmenity;
use App\Models\PropertyImage;

use Illuminate\Support\Facades\Storage;

class PropertyController extends Controller
{
    public function index(){
        $propeties = Property::all();

        return $propeties->load('user', 'amenities', 'gallery');
    }

    public function show(Property $property){
        return $property->load('user', 'amenities', 'gallery');
    }

    public function store(Request $request){
        //getAuthenticatedUser
        $property = Property::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'property_type' => $request->property_type,
            'operation' => $request->operation,
            'state' => $request->state,
            'city' => $request->city,
            'neighborhood' => $request->neighborhood,
            'cp' => $request->cp,
            'street' => $request->street,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'num_bathrooms' => $request->num_bathrooms,
            'bedrooms' => $request->bedrooms,
            'm2_construction' => $request->m2_construction,
            'parking' => $request->parking,
            'age' => $request->age,
            'departments' => $request->departments,
            'floor' => $request->floor,
            'public_key' => '',
            'user' => auth()->user()->id,
        ]);

        $public_key = 'PCOM-' . $property->id . '/' . date('y');

        $property->update(['public_key' => $public_key]);
        

        $amenities = is_array($request->amenities)? $request->amenities:[];
        

        if(count($amenities)){
            foreach($amenities as $amenity){
                $new_property_amenity = new PropertyAmenity();
                $new_property_amenity->property_id = $property->id;
                $new_property_amenity->amenity_id = $amenity;
                $new_property_amenity->save();
            }
        }

        if ($request->file) {
            foreach($request->file as $picture){
                $property_image = new PropertyImage();
                $property_image->property = $property->id;
                $property_image->path = $picture->store('property_images', 'public');
                $property_image->order = PropertyImage::where('property', $property->id)->max('order') + 1;
                $property_image->save();
            }
        }


        return response()->json($request, 201);
        //$property = Property::create($request->all());
        //return response()->json($property, 201);
    }

    public function update(Request $request, Property $property){
        //return response()->json($request, 200);
        $property->update($request->all());
        $amenities = is_array($request->amenities)? $request->amenities:[];
        $send_ameninites = [];

        foreach($amenities as $amenity){
            array_push($send_ameninites, $amenity);
        }

        $property_amenities = PropertyAmenity::where('property_id', $property->id)->get();

        $ids_amenities = [];

        foreach($property_amenities as $amenity){
            array_push($ids_amenities, $amenity->amenity_id);
        }
        
        $new_amenities = array_diff($send_ameninites, $ids_amenities);
        $delete_amenities = array_diff($ids_amenities, $send_ameninites);
        
        
        if(count($new_amenities)){
            foreach($new_amenities as $amenity){
                $new_property_amenity = new PropertyAmenity();
                $new_property_amenity->property_id = $property->id;
                $new_property_amenity->amenity_id = $amenity;
                $new_property_amenity->save();
            }
        }

        if (count($delete_amenities)) {
            PropertyAmenity::where('property_id', $property->id)->where('amenity_id', $delete_amenities)->delete();
        }

        if ($request->file) {
            foreach($request->file as $picture){
                $property_image = new PropertyImage();
                $property_image->property = $property->id;
                $property_image->path = $picture->store('property_images', 'public');
                $property_image->order = PropertyImage::where('property', $property->id)->max('order') + 1;
                $property_image->save();
            }
        }
        


        return response()->json($property, 200);
    }

    public function delete(Property $property){
        $property_image = PropertyImage::where('property', $property->id)->get();

        foreach($property_image as $image){
            Storage::disk('public')->delete($image->path);
        }
        PropertyImage::where('property', $property->id)->delete();
        PropertyAmenity::where('property_id', $property->id)->delete();
        $property->delete();
        return response()->json(null, 204);
    }

    public function deleteFile(PropertyImage $image){
        Storage::disk('public')->delete($image->path);
        $image->delete();
        //exit;
        return response()->json(null, 204);
    }

}
