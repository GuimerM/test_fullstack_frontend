import React from 'react'
import { Outlet, Navigate } from 'react-router'

const Public = () => {

  const userLogged = localStorage.getItem('token')
  //const userLogged = false

  if ( userLogged ) {
    return <Navigate to="/" />
  }
  return <Outlet />
  
}

export default Public