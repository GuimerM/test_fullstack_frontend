import React from 'react'
import { Outlet, Navigate } from 'react-router'

const Protected = () => {

  const userLogged = localStorage.getItem('token')
  //const userLogged = false

  if ( !userLogged ) {
    return <Navigate to="/login" />
  }
  return <Outlet />
  
}

export default Protected