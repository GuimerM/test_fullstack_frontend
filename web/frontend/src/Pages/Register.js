import React, { useState } from "react";
import { Row, Col } from "react-flexbox-grid";
import { Card, Form, Button, Alert } from "react-bootstrap";
import { Link } from "react-router-dom";
import Axios from "axios";

const { Body } = Card;
const { Control, Group: GroupControl, Label: LabelForm } = Form;

const API_URL = process.env.REACT_APP_API_URL;

const register = (values) => {
  const { email, password, name } = values;
  const data = {
    email,
    password,
    name
  };

  Axios.post(`${API_URL}/auth/register/`, data)
    .then((r) => {
      localStorage.setItem("token", r.data.access_token);
      window.location = "/";
    })
    .catch((e) => {
      alert("Error al iniciar sesión");
    });
};

const RegisterPage = () => {
  const [showMessage, setShowMessage] = useState(false);
  const [values, setValues] = useState({
    email: "",
    password: "",
    name:""
  });
  const [formerrors, setFormErrors] = useState({});

  const handleChange = (event) => {
    setValues((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const validateForm = () => {
    let errors = {};

    //password field
    if (!values.name) {
      errors.name = "El nombre es requerida.";
    }

    //password field
    if (!values.password) {
      errors.pawssword = "la contraseña es requerida.";
    }

    //email field
    if (!values.email) {
      errors.email = "Correo electronico es requerido.";
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
      errors.email = "El correo electronico es invalido.";
    }

    setFormErrors(errors);

    if (Object.keys(errors).length === 0) {
      return true;
    } else {
      return false;
    }
  };

  const handleSubmitFom = (event) => {
    if (event) event.preventDefault();
    if (validateForm(values)) {
      setShowMessage(true);
      register(values);
    } else {
      setShowMessage(false);
    }
  };

  return (
    <div className="login-page">
      <div className="bg-login" />
      <div className="root-login">
        <Row around="xs" middle="xs" className="content-login">
          <Col xs={12} sm={4}>
            <Card>
              <Body className="body-login">
                <Form onSubmit={handleSubmitFom}>
                  <Row>
                    <Col>
                      <Row>
                        <Col>
                          <Alert show={showMessage} variant="success">
                            <Alert.Heading>Exito!</Alert.Heading>
                            <p>Sesión iniciada correctament</p>
                            <hr />
                            <div className="d-flex justify-content-end">
                              <Button
                                onClick={() => setShowMessage(false)}
                                variant="outline-success"
                              >
                                Cerrar
                              </Button>
                            </div>
                          </Alert>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs={12}>
                      <div
                        style={{ textAlign: "center", marginBottom: "2rem" }}
                      >
                        <h3>Logo OKOL</h3>
                      </div>
                    </Col>
                    <Col xs={12}>
                      <Row>
                        <Col xs={12}>
                          <GroupControl>
                            <LabelForm>Nombre:</LabelForm>
                            <Control
                              type="text"
                              placeholder="Ingrese su nombre."
                              name="name"
                              value={values.name}
                              onChange={handleChange}
                            />
                            {formerrors.name && (
                              <p className="text-warning">{formerrors.name}</p>
                            )}
                          </GroupControl>
                        </Col>
                        <Col xs={12}>
                          <GroupControl>
                            <LabelForm>Email:</LabelForm>
                            <Control
                              type="email"
                              placeholder="Ingrese su correo."
                              name="email"
                              value={values.email}
                              onChange={handleChange}
                            />
                            {formerrors.email && (
                              <p className="text-warning">
                                {formerrors.email}
                              </p>
                            )}
                          </GroupControl>
                        </Col>
                        <Col xs={12}>
                          <GroupControl>
                            <LabelForm>Contraseña:</LabelForm>
                            <Control
                              type="password"
                              placeholder="Ingrese su contraseña."
                              name="password"
                              value={values.password}
                              onChange={handleChange}
                            />
                            {formerrors.password && (
                              <p className="text-warning">
                                {formerrors.password}
                              </p>
                            )}
                          </GroupControl>
                        </Col>
                        <Col xs={12}>
                          <div
                            className="d-grid gap-2"
                            style={{ marginTop: "3rem" }}
                          >
                            <Button variant="primary" size="lg" type="submit">
                              Registrarse
                            </Button>
                          </div>
                        </Col>
                        <Col xs={12}>
                          <div style={{ marginTop: "3rem" }}>
                            <span style={{ display: "inline" }}>
                              ¿Ya tienes una tienes cuenta?
                            </span>{" "}
                            <Link className="navigate-link" to="/register">
                              Inicia Sesión
                            </Link>
                          </div>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Form>
              </Body>
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default RegisterPage;
