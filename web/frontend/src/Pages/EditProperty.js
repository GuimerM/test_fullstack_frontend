import React, { useEffect, useState, Fragment } from "react";
import { Container, Card } from "react-bootstrap";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import store from "../redux/store";
import { getAllAmenities, getProperty } from "../redux/actionCreators";
import NavigationBar from "../Components/Navbar";
import FormAddProperty from "../Components/Forms/FormAddProperty";
import Axios from "axios";
const FormData = require("form-data");

const API_URL = process.env.REACT_APP_API_URL;

const editProperty = (values, id) => {
  const token = localStorage.getItem("token");
  const form = new FormData();
  const { gallery, amenities, ...restData } = values;
  //console.log(gallery);
  form.append("_method", "PUT");
  for (let i in restData) {
    form.append(i, restData[i]);
  }

  for (let i in amenities) {
    form.append("amenities[]", amenities[i].id);
  }

  for (let i in gallery) {
    if (gallery[i].hasOwnProperty("done")) {
      if (!gallery[i].done) {
        //console.log("file",gallery[i].file)
        form.append("file[]", gallery[i].file);
      }
    }
  }

  Axios.post(`${API_URL}/auth/properties/${id}`, form, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
  })
    .then((r) => {
      window.location = "/";
    })
    .catch((e) => {
      alert("Error al editar");
    });
};

const EditPropertyPage = ({ property, amenities }) => {
  const { id: idProperty } = useParams();

  useEffect(() => {
    store.dispatch(getProperty(idProperty));
    store.dispatch(getAllAmenities());
  }, []);

  const userLogged = localStorage.getItem("token");

  const onSubmitForm = (data) => {
    editProperty(data, idProperty);
  };

  return (
    <div>
      <NavigationBar userLogged={userLogged} />
      <br />
      <br />
      <Container>
        <Card>
          <Card.Body>
            {property && Object.keys(property).length > 0 && (
              <FormAddProperty
                action="EDIT"
                dataProperty={property}
                amenities={amenities}
                onSaveProperty={onSubmitForm}
              />
            )}
          </Card.Body>
        </Card>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  property: state.propertyReducer.property,
  amenities: state.amenityReducer.amenities,
});

export default connect(mapStateToProps, {})(EditPropertyPage);
