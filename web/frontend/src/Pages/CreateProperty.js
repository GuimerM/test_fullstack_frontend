import React, { useEffect, useState, Fragment } from "react";
import { Container, Card } from "react-bootstrap";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import store from "../redux/store";
import { getAllAmenities, getProperty } from "../redux/actionCreators";
import NavigationBar from "../Components/Navbar";
import FormAddProperty from "../Components/Forms/FormAddProperty";
import Axios from "axios";
const FormData = require("form-data");

const API_URL = process.env.REACT_APP_API_URL;

const addProperty = (values, id) => {
  const token = localStorage.getItem("token");
  const form = new FormData();
  const { gallery, amenities, ...restData } = values;
  
  for (let i in restData) {
    form.append(i, restData[i]);
  }

  for (let i in amenities) {
    form.append("amenities[]", amenities[i].id);
  }

  for (let i in gallery) {
    if (gallery[i].hasOwnProperty("done")) {
      if (!gallery[i].done) {
        //console.log("file",gallery[i].file)
        form.append("file[]", gallery[i].file);
      }
    }
  }

  Axios.post(`${API_URL}/auth/properties`, form, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
  })
    .then((r) => {
      window.location = "/";
    })
    .catch((e) => {
      alert("Error al crear propiedad");
    });
};

const CreateProperty = ({ amenities }) => {
  const { id: idProperty } = useParams();

  useEffect(() => {
    
    store.dispatch(getAllAmenities());
  }, []);

  const userLogged = localStorage.getItem("token");

  const onSubmitForm = (data) => {
    addProperty(data, idProperty);
  };
  return (
    <div>
      <NavigationBar userLogged={userLogged} />
      <br />
      <br />
      <Container>
        <Card>
          <Card.Body>
            <FormAddProperty
              action="ADD"
              amenities={amenities}
              onSaveProperty={onSubmitForm}
            />
          </Card.Body>
        </Card>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  amenities: state.amenityReducer.amenities,
});

export default connect(mapStateToProps, {})(CreateProperty);
