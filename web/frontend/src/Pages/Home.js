import React, { useEffect, useState } from "react";
import { Col, Row, Container, Button } from "react-bootstrap";
// import { Grid, Row, Col } from 'react-flexbox-grid';
import NavigationBar from "../Components/Navbar";
import { connect } from "react-redux";

import store from "../redux/store";
import { getAllProperties } from "../redux/actionCreators";
import CardProperty from "../Components/CardProperty";
import Mapa from "../Components/GoogleMap";
import { Link } from "react-router-dom";
import ModalDeleteProperty from "../Components/Modals/ModalDeleteProperty";
import Axios from "axios";

const API_URL = process.env.REACT_APP_API_URL;

const deleteProperty = (id) => {
  Axios.delete(`${API_URL}/auth/properties/${id}`)
    .then((r) => {
      localStorage.setItem("token", r.data.access_token);
      window.location = "/";
    })
    .catch((e) => {
      alert("Error al eliminar");
    });
};

const HomePage = ({ properties }) => {
  const userLogged = localStorage.getItem("token");
  useEffect(() => {
    store.dispatch(getAllProperties());
  }, []);

  const [show, setShow] = useState(false);
  const [idDelete, setIdDelete] = useState("");

  const isLoggin = userLogged ? true : false;

  const firstProperty =
    properties && Array.isArray(properties) ? properties[0] : {};

  const onDeleteProperty = () => {
    deleteProperty(idDelete);
  };

  const onClickDelete = (id) => {
    setShow(true);
    setIdDelete(id);
  };

  const onCloseModalDelete = () => {
    setShow(false);
    setIdDelete("");
  };

  return (
    <div>
      <NavigationBar userLogged={userLogged} />
      <ModalDeleteProperty
        show={show}
        idProperty={idDelete}
        onDeleteProperty={onDeleteProperty}
        handleClose={onCloseModalDelete}
      />
      <Container>
        <div className="content-properties">
          <Container>
            <Row className="content-mapa">
              <Col>
                {properties ? (
                  <div>
                    <Mapa
                      properties={properties}
                      initialCenter={{
                        lat: firstProperty.latitude,
                        lng: firstProperty.longitude,
                      }}
                      zoom={13}
                    />
                  </div>
                ) : (
                  <span>loading...</span>
                )}
              </Col>
            </Row>
          </Container>
          <Container>
            {isLoggin && (
              <Row>
                <Col style={{ textAlign: "right", marginBottom: "2rem" }}>
                  <Link to="/crear-propiedad">
                    <Button variant="success">Agregar Propiedad</Button>
                  </Link>
                </Col>
              </Row>
            )}
            {properties ? (
              <Row md={3} sm={2} xs={1}>
                {properties.map((property) => (
                  <Col key={property.id}>
                    <CardProperty
                      property={property}
                      isLoggin={isLoggin}
                      onDeleteProperty={onClickDelete}
                    />
                  </Col>
                ))}
              </Row>
            ) : (
              <Row md={3} sm={2} xs={1}></Row>
            )}
          </Container>
        </div>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  properties: state.propertyReducer.properties,
});

export default connect(mapStateToProps, {})(HomePage);
