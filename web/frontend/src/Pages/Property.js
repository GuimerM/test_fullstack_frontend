import React, { useEffect } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import { RiHotelBedLine, RiParkingBoxLine } from "react-icons/ri";
import { FaShower } from "react-icons/fa";
import { BiArea } from "react-icons/bi";
import { BsCalendarDate } from "react-icons/bs";
import { MdHeight } from "react-icons/md";
import { GiGardeningShears } from "react-icons/gi";
import { MdApartment } from "react-icons/md";

import store from "../redux/store";
import { getProperty } from "../redux/actionCreators";
import { Col, Row, Container, Card } from "react-bootstrap";
import NavigationBar from "../Components/Navbar";
import { getOperation, getPropertyType } from "../Components/CardProperty";
import Mapa from "../Components/GoogleMap";
import SliderProperty from '../Components/SliderProperty'

const API_URL_FILE = process.env.REACT_APP_IMAGE_URL;
const PropertyPage = ({ property }) => {
  const { id: idProperty } = useParams();

  useEffect(() => {
    store.dispatch(getProperty(idProperty));
  }, []);

  const gallery =
    property && Object.keys(property).length > 0 ? property.gallery : [];
  const image1 =
    gallery && Array.isArray(gallery) && gallery.length > 0
      ? `${API_URL_FILE}/storage/${gallery[0].path}`
      : "";

  const userLogged = localStorage.getItem("token");

  return (
    <div>
      <NavigationBar userLogged={userLogged} />
      <div className="content-carrousel">
        <div
          className="back-property"
          style={{ backgroundImage: `url(${image1})` }}
        >
          <div className="bottom-gallery">
            {property && Object.keys(property).length > 0 && (
              <div className="">
                <Container>
                  <Row className="content-mapa">
                    <Col>
                      <p className="sale" style={{ marginBottom: "10px" }}>
                        {getPropertyType(property.property_type)} en{" "}
                        {getOperation(property.operation)}
                      </p>
                      <h1>{property.name}</h1>
                      <div>
                        Colonia {property.neighborhood} {property.city}, CP {property.cp},{" "}
                        {property.state}
                      </div>
                      <div className="price-gallery">$ {property.price} MN</div>
                    </Col>
                    <Col xs={12} className="gallery-slider"></Col>
                  </Row>
                </Container>
                <br />
                <div className="slider-content">
                  <Container>
                    <SliderProperty gallery={property.gallery} />
                  </Container>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="content-property-detail">
        <Container>
          <Row>
            <Col>
              {property && Object.keys(property).length > 0 && (
                <div className="property-detail">
                  <Card>
                    <Card.Body>
                      <Container>
                        <Row xs={1}>
                          <Col>
                            <h2 className="title">{property.name}</h2>
                          </Col>
                          <Col>
                            <div className="price">$ {property.price} MN</div>
                          </Col>
                          <Col>
                            <div className="type-property">
                              {getPropertyType(property.property_type)} en{" "}
                              {getOperation(property.operation)}
                            </div>
                          </Col>
                          <Col>
                            <div>{property.description}</div>
                          </Col>
                        </Row>
                      </Container>
                      <Container className="property-feartures">
                        <Row>
                          <Col xs={12}>
                            <h3 className="subtitle">
                              Caracteristicas del inmueble
                            </h3>
                          </Col>
                          <Col md={6} xs={12}>
                            <Container className="feature-items">
                              <Row sm={3} xs={2}>
                                <Col>
                                  <div>
                                    <RiHotelBedLine
                                      color="#4ad200"
                                      size="2rem"
                                    />
                                  </div>
                                  <div>
                                    Recámaras
                                    <br />{property.bedrooms}
                                  </div>
                                </Col>
                                <Col>
                                  <div>
                                    <FaShower color="#4ad200" size="2rem" />
                                  </div>
                                  <div>
                                    Baños
                                    <br />{property.num_bathrooms}
                                  </div>
                                </Col>
                                <Col>
                                  <div>
                                    <BiArea color="#4ad200" size="2rem" />
                                  </div>
                                  <div>
                                    Tamaño de construccion
                                    <br />{property.m2_construction}
                                  </div>
                                </Col>
                                
                                <Col>
                                  <div>
                                    <RiParkingBoxLine
                                      color="#4ad200"
                                      size="2rem"
                                    />
                                  </div>
                                  <div>
                                    Estacionamiento
                                    <br />{property.parking}
                                  </div>
                                </Col>
                                <Col>
                                  <div>
                                    <BsCalendarDate
                                      color="#4ad200"
                                      size="2rem"
                                    />
                                  </div>
                                  <div>
                                    Edad del inmueble
                                    <br />{property.age}
                                  </div>
                                </Col>
                                <Col>
                                  <div>
                                    <MdHeight color="#4ad200" size="2rem" />
                                  </div>
                                  <div>
                                    No. de pisos
                                    <br />{property.floor}
                                  </div>
                                </Col>
                                <Col>
                                  <div>
                                    <MdApartment
                                      color="#4ad200"
                                      size="2rem"
                                    />
                                  </div>
                                  <div>
                                    Jardin
                                    <br />{property.departaments}
                                  </div>
                                </Col>
                              </Row>
                            </Container>
                          </Col>
                        </Row>
                      </Container>
                      <Container className="property-feartures">
                        <Row>
                          <Col xs={12}>
                            <h3 className="subtitle">Ubicación</h3>
                          </Col>
                          <Col xs={12}>
                            <Mapa
                              properties={[property]}
                              initialCenter={{
                                lat: property.latitude,
                                lng: property.longitude,
                              }}
                              zoom={18}
                            />
                          </Col>
                        </Row>
                      </Container>
                    </Card.Body>
                  </Card>
                </div>
              )}
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  property: state.propertyReducer.property,
});

export default connect(mapStateToProps, {})(PropertyPage);
