import React from "react";
import { Modal, Button } from "react-bootstrap";

const ModalDeleteProperty = ({ show, handleClose, onDeleteProperty }) => {
  return(
    <Modal show={show} onHide={handleClose}>
    <Modal.Header closeButton>
      <Modal.Title>Eliminar Propiedad</Modal.Title>
    </Modal.Header>
    <Modal.Body>¿Seguro de eliminar esta propiedad?</Modal.Body>
    <Modal.Footer>
      <Button variant="secondary" onClick={handleClose}>
        Cancelar
      </Button>
      <Button variant="warning" onClick={onDeleteProperty}>
        Eliminar
      </Button>
    </Modal.Footer>
  </Modal>
  )
};

export default ModalDeleteProperty;
