import React from "react";
import Slider from "react-slick";

const API_URL_FILE = process.env.REACT_APP_IMAGE_URL;

const settings = {
  dots: false,
  arrows: true,
  infinite: false,
  speed: 500,
  slidesToScroll: 0,
  slidesToShow: 4,
  focusOnSelect: true,
  accessibility: true,
  //nextArrow: <SampleNextArrow />,
  //prevArrow: <SamplePrevArrow />,
  //nextArrow: <button type="button" data-role="none" className="slick-arrow slick-prev slick-disabled" style="display: block;"> Previous</button>,
  slickNext: true,
};

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "red" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "green" }}
      onClick={onClick}
    />
  );
}

const SliderProperty = ({ gallery }) => {
  return (
    <div>
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-prev slick-disabled"
        style={{display: "block"}}
      >
        {" "}
        Previous
      </button>
      <Slider {...settings}>
        {gallery &&
          Array.isArray(gallery) &&
          gallery.length > 0 &&
          gallery.map((image) => (
            <div>
              <img src={`${API_URL_FILE}/storage/${image.path}`} width={118} height={79} alt={image.path} />
            </div>
          ))}
      </Slider>
    </div>
  );
};

export default SliderProperty;
