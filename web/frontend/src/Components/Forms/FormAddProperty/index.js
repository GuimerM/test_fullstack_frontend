import React, { useState, useEffect } from "react";
import {
  Col,
  Row,
  Container,
  Form,
  Card,
  Button,
  Alert,
} from "react-bootstrap";
import RUG, { Card as CardU, DropArea } from "react-upload-gallery";
import { useForm } from "react-hook-form";
import Axios from "axios";
import "react-upload-gallery/dist/style.css";

import Mapa from "../../GoogleMap";

const FormGroup = Form.Group;
const FormLabel = Form.Label;
const FormControl = Form.Control;
const FormSelect = Form.Select;

const API_URL_FILE = process.env.REACT_APP_IMAGE_URL;
const API_URL = process.env.REACT_APP_API_URL;

const FormAddProperty = ({
  dataProperty,
  amenities: allAmenities,
  action,
  onSaveProperty,
}) => {
  const [imagesGallery, setImagesGallery] = useState([]);
  const [amenities, setAmenities] = useState([]);
  const [loadedData, setLoadedData] = useState(false);
  const [ubication, setUbication] = useState({});

  useEffect(
    function () {
      if (dataProperty) {
        setUbication({
          lat: dataProperty.latitude,
          lng: dataProperty.longitude,
        });
        const images = dataProperty.gallery.map((image) => ({
          source: `${API_URL_FILE}/storage/${image.path}`,
          name: `${image.order}`,
          idImage: image.id,
          size: "",
        }));
        setImagesGallery(images);
        setAmenities(dataProperty.amenities);
      }

      setLoadedData(true);
    },
    [dataProperty]
  );

  const {
    register,
    formState: { errors },
    handleSubmit,
    setValue,
  } = useForm({
    defaultValues: dataProperty,
  });

  //setValue("name", dataProperty.name);

  const onSubmitForm = (data, e) => {
    e.preventDefault();
    const {
      name,
      description,
      operation,
      property_type,
      price,
      num_bathrooms,
      bedrooms,
      m2_construction,
      parking,
      age,
      floor,
      departments,
      state,
      city,
      neighborhood,
      cp,
      street,
    } = data;

    const dataSend = {
      name,
      description,
      operation,
      property_type,
      price,
      num_bathrooms,
      bedrooms,
      m2_construction,
      parking,
      age,
      floor,
      departments,
      state,
      city,
      neighborhood,
      cp,
      street,
      latitude: ubication.lat,
      longitude: ubication.lng,
      amenities: amenities,
      gallery: imagesGallery,
    };

    //console.log("entro", dataSend);
    onSaveProperty(dataSend);
  };

  const initialCenter =
    dataProperty && Object.keys(dataProperty).length > 0
      ? { lng: dataProperty.longitude, lat: dataProperty.latitude }
      : {};

  const onMoveMarker = (data) => {
    setUbication(data);
  };

  const addAmenity = (data) => {
    const id = data.target.value;
    if (id !== "") {
      const searchAmenity = amenities.filter(
        (amenity) => amenity.id === Number(id)
      );

      if (searchAmenity.length > 0) return;
      const amenityData = allAmenities.filter(
        (amenity) => amenity.id === Number(id)
      );

      setAmenities([...amenities, amenityData[0]]);
    }
  };

  const removeAmenity = (id) => {
    const newAmenities = amenities.filter((amenity) => amenity.id !== id);
    setAmenities(newAmenities);
  };

  const propsMap = {
    zoom: 18,
    propertyEdit: ubication,
    initialCenter: initialCenter,
    onMoveMarker: onMoveMarker,
  };
  if (action === "ADD") {
    propsMap.createMarker = onMoveMarker;
    propsMap.zoom = 5;
  }
  //console.log(ubication);

  const onDeleteImageGallery = (image) => {
    const token = localStorage.getItem("token");
    console.log(image);
    if (image.hasOwnProperty("idImage")) {
      const id = image.idImage;
      Axios.delete(`${API_URL}/auth/image/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((r) => {})
        .catch((e) => {
          alert("Error al eliminar imagen");
        });
    }
  };

  return (
    <Form onSubmit={handleSubmit(onSubmitForm)}>
      <Row>
        <Col xs={12} md={{ span: 8, offset: 2 }}>
          <Container>
            <Row className="mb-3">
              {action === "EDIT" && (
                <Col xs={12}>
                  <FormGroup>
                    <FormLabel>Clave Pública:</FormLabel>
                    <FormControl
                      type="text"
                      disabled
                      value={dataProperty.public_key}
                    />
                  </FormGroup>
                </Col>
              )}
              <Col xs={12}>
                <FormGroup>
                  <FormLabel>Titulo de la propiedad:</FormLabel>
                  <FormControl
                    type="text"
                    name="name"
                    {...register("name", {
                      required: true,
                      pattern: /^[A-ZÁÉÍÓÚÑ ]+$/i,
                    })}
                  />
                  {errors?.name?.type === "required" && (
                    <span className="text-warning">
                      Este campo es requerido.
                    </span>
                  )}
                  {errors?.name?.type === "pattern" && (
                    <span className="text-warning">
                      Este campo solo admite texto.
                    </span>
                  )}
                </FormGroup>
              </Col>
              <Col xs={12}>
                <FormGroup>
                  <FormLabel>Descripción:</FormLabel>
                  <FormControl
                    as="textarea"
                    rows={3}
                    name="description"
                    {...register("description", {
                      required: true,
                    })}
                  />
                  {errors?.description && (
                    <span className="text-warning">
                      Este campo es requerido.
                    </span>
                  )}
                </FormGroup>
              </Col>
              <Col xs={12} md={6}>
                <FormGroup>
                  <FormLabel>Operación:</FormLabel>
                  <FormSelect
                    name="operation"
                    {...register("operation", {
                      required: true,
                    })}
                  >
                    <option value="SALE">Venta</option>
                    <option value="RENT">Renta</option>
                  </FormSelect>
                  {errors?.operation && (
                    <span className="text-warning">
                      Este campo es requerido.
                    </span>
                  )}
                </FormGroup>
              </Col>
              <Col xs={12} md={6}>
                <FormGroup>
                  <FormLabel>Tipo de inmueble:</FormLabel>
                  <FormSelect
                    name="property_type"
                    {...register("property_type", {
                      required: true,
                    })}
                  >
                    <option value="HOUSE">Casa</option>
                    <option value="APARTAMENT">Departamento</option>
                    <option value="TERRAIN">Terreno</option>
                    <option value="OFFICE">Oficina</option>
                    <option value="LOCAL">Local</option>
                  </FormSelect>
                  {errors?.property_type && (
                    <span className="text-warning">
                      Este campo es requerido.
                    </span>
                  )}
                </FormGroup>
              </Col>

              <Col xs={12} md={6}>
                <FormGroup>
                  <FormLabel>Precio:</FormLabel>
                  <FormControl
                    type="number"
                    min={0}
                    name="price"
                    {...register("price", {
                      required: true,
                      valueAsNumber: true,
                      value: 0,
                      min: 0,
                    })}
                  />
                  {errors?.price?.type === "required" && (
                    <span className="text-warning">
                      Este campo es requerido.
                    </span>
                  )}
                  {errors?.price?.type === "valueAsNumber" && (
                    <span className="text-warning">
                      Este campo solo acepta numero.
                    </span>
                  )}
                </FormGroup>
              </Col>

              <Col xs={12}>
                <Row>
                  <Col xs={12} md={4}>
                    <FormGroup>
                      <FormLabel>Número de baños:</FormLabel>
                      <FormControl
                        type="number"
                        min={0}
                        name="num_bathrooms"
                        {...register("num_bathrooms", {
                          required: true,
                          valueAsNumber: true,
                          value: 0,
                          min: 0,
                        })}
                      />
                      {errors?.num_bathrooms?.type === "required" && (
                        <span className="text-warning">
                          Este campo es requerido.
                        </span>
                      )}
                      {errors?.num_bathrooms?.type === "valueAsNumber" && (
                        <span className="text-warning">
                          Este campo solo acepta numero.
                        </span>
                      )}
                    </FormGroup>
                  </Col>
                  <Col xs={12} md={4}>
                    <FormGroup>
                      <FormLabel>Número de recamaras:</FormLabel>
                      <FormControl
                        type="number"
                        min={0}
                        name="bedrooms"
                        {...register("bedrooms", {
                          required: true,
                          valueAsNumber: true,
                          value: 0,
                          min: 0,
                        })}
                      />
                      {errors?.bedrooms?.type === "required" && (
                        <span className="text-warning">
                          Este campo es requerido.
                        </span>
                      )}
                      {errors?.bedrooms?.type === "valueAsNumber" && (
                        <span className="text-warning">
                          Este campo solo acepta numero.
                        </span>
                      )}
                    </FormGroup>
                  </Col>
                  <Col xs={12} md={4}>
                    <FormGroup>
                      <FormLabel>Tamaño de la contrucción(m2):</FormLabel>
                      <FormControl
                        type="number"
                        min={0}
                        name="m2_construction"
                        {...register("m2_construction", {
                          required: true,
                          valueAsNumber: true,
                          value: 0,
                          min: 0,
                        })}
                      />
                      {errors?.m2_construction?.type === "required" && (
                        <span className="text-warning">
                          Este campo es requerido.
                        </span>
                      )}
                      {errors?.m2_construction?.type === "valueAsNumber" && (
                        <span className="text-warning">
                          Este campo solo acepta numero.
                        </span>
                      )}
                    </FormGroup>
                  </Col>
                  <Col xs={12} md={4}>
                    <FormGroup>
                      <FormLabel>Número de estacionamientos:</FormLabel>
                      <FormControl
                        type="number"
                        min={0}
                        name="parking"
                        {...register("parking", {
                          required: true,
                          valueAsNumber: true,
                          value: 0,
                          min: 0,
                        })}
                      />
                      {errors?.parking?.type === "required" && (
                        <span className="text-warning">
                          Este campo es requerido.
                        </span>
                      )}
                      {errors?.parking?.type === "valueAsNumber" && (
                        <span className="text-warning">
                          Este campo solo acepta numero.
                        </span>
                      )}
                    </FormGroup>
                  </Col>
                  <Col xs={12} md={4}>
                    <FormGroup>
                      <FormLabel>Edad del inmueble(años):</FormLabel>
                      <FormControl
                        type="number"
                        min={0}
                        name="age"
                        {...register("age", {
                          required: true,
                          valueAsNumber: true,
                          value: 0,
                          min: 0,
                        })}
                      />
                      {errors?.age?.type === "required" && (
                        <span className="text-warning">
                          Este campo es requerido.
                        </span>
                      )}
                      {errors?.age?.type === "valueAsNumber" && (
                        <span className="text-warning">
                          Este campo solo acepta numero.
                        </span>
                      )}
                    </FormGroup>
                  </Col>
                  <Col xs={12} md={4}>
                    <FormGroup>
                      <FormLabel>Numero de pisos:</FormLabel>
                      <FormControl
                        type="number"
                        min={0}
                        name="floor"
                        {...register("floor", {
                          required: true,
                          valueAsNumber: true,
                          value: 0,
                          min: 0,
                        })}
                      />
                      {errors?.floor?.type === "required" && (
                        <span className="text-warning">
                          Este campo es requerido.
                        </span>
                      )}
                      {errors?.floor?.type === "valueAsNumber" && (
                        <span className="text-warning">
                          Este campo solo acepta numero.
                        </span>
                      )}
                    </FormGroup>
                  </Col>
                  <Col xs={12} md={4}>
                    <FormGroup>
                      <FormLabel>Numero de departamentos:</FormLabel>
                      <FormControl
                        type="number"
                        min={0}
                        name="departments"
                        {...register("departments", {
                          required: true,
                          valueAsNumber: true,
                          value: 0,
                          min: 0,
                        })}
                      />
                      {errors?.departments?.type === "required" && (
                        <span className="text-warning">
                          Este campo es requerido.
                        </span>
                      )}
                      {errors?.departments?.type === "valueAsNumber" && (
                        <span className="text-warning">
                          Este campo solo acepta numero.
                        </span>
                      )}
                    </FormGroup>
                  </Col>
                </Row>
              </Col>

              <Col xs={12}>
                <br />
                <h4>Amenidades</h4>
              </Col>
              <Col xs={12} md={6}>
                <FormGroup>
                  <FormLabel>Seleccione amenidad:</FormLabel>
                  <FormSelect onChange={(value) => addAmenity(value)}>
                    <option value="">Seleccione amenidad</option>
                    {allAmenities &&
                      Array.isArray(allAmenities) &&
                      allAmenities.map((amenity) => (
                        <option value={amenity.id}>{amenity.name}</option>
                      ))}
                  </FormSelect>
                </FormGroup>
              </Col>
              <Col xs={12}>
                <br />
                <Card>
                  <Card.Body>
                    <Container>
                      <Row>
                        {amenities &&
                          amenities.map((amenity) => (
                            <Col xs="auto">
                              <Alert
                                variant="dark"
                                onClose={() => {
                                  removeAmenity(amenity.id);
                                }}
                                dismissible
                              >
                                {amenity.name}
                              </Alert>
                            </Col>
                          ))}
                      </Row>
                    </Container>
                  </Card.Body>
                </Card>
              </Col>
              <Col xs={12}>
                <br />
                <h4>Datos de ubicación</h4>
              </Col>
              <Col xs={12} md={4}>
                <FormGroup>
                  <FormLabel>Estado:</FormLabel>
                  <FormControl
                    type="text"
                    name="state"
                    {...register("state", {
                      required: true,
                    })}
                  />
                  {errors?.state && (
                    <span className="text-warning">
                      Este campo es requerido.
                    </span>
                  )}
                </FormGroup>
              </Col>
              <Col xs={12} md={4}>
                <FormGroup>
                  <FormLabel>Delegación o municipio:</FormLabel>
                  <FormControl
                    type="text"
                    name="city"
                    {...register("city", {
                      required: true,
                    })}
                  />
                  {errors?.city && (
                    <span className="text-warning">
                      Este campo es requerido.
                    </span>
                  )}
                </FormGroup>
              </Col>
              <Col xs={12} md={4}>
                <FormGroup>
                  <FormLabel>Colonia:</FormLabel>
                  <FormControl
                    type="text"
                    name="neighborhood"
                    {...register("neighborhood", {
                      required: true,
                    })}
                  />
                  {errors?.neighborhood && (
                    <span className="text-warning">
                      Este campo es requerido.
                    </span>
                  )}
                </FormGroup>
              </Col>
              <Col xs={12} md={4}>
                <FormGroup>
                  <FormLabel>C.P.:</FormLabel>
                  <FormControl
                    type="text"
                    name="cp"
                    {...register("cp", {
                      required: true,
                    })}
                  />
                  {errors?.cp && (
                    <span className="text-warning">
                      Este campo es requerido.
                    </span>
                  )}
                </FormGroup>
              </Col>
              <Col xs={12} md={8}>
                <FormGroup>
                  <FormLabel>Calle y número:</FormLabel>
                  <FormControl
                    type="text"
                    name="street"
                    {...register("street", {
                      required: true,
                    })}
                  />
                  {errors?.street && (
                    <span className="text-warning">
                      Este campo es requerido.
                    </span>
                  )}
                </FormGroup>
              </Col>
              <Col xs={12}>
                <br />
                <FormGroup>
                  <FormLabel>
                    Seleccione un punto en el mapa para obtener ubicación del
                    inmueble:
                  </FormLabel>
                </FormGroup>

                <div>
                  <Mapa {...propsMap} />
                </div>
              </Col>

              <Col xs={12}>
                <br />
                <h4>Galería</h4>
                <Card>
                  <Card.Body>
                    {false && (
                      <RUG
                        action="http://example.com/upload"
                        ssrSupport
                        //autoUpload={false}
                        //source={(response) => response.source} // response image source
                        initialState={imagesGallery}
                        /**/
                      />
                    )}
                    {loadedData && (
                      <RUG
                        autoUpload={false}
                        onChange={(images) => {
                          setImagesGallery(images);
                        }}
                        header={({ openDialogue }) => (
                          <DropArea>
                            {(isDrag) => (
                              <div
                                style={{
                                  background: isDrag ? "yellow" : "#fff",
                                }}
                              >
                                <Button onClick={openDialogue}>
                                  Seleccionar Imagen
                                </Button>
                              </div>
                            )}
                          </DropArea>
                        )}
                        initialState={imagesGallery}
                        onDeleted={(image) => onDeleteImageGallery(image)}
                      ></RUG>
                    )}
                  </Card.Body>
                </Card>
              </Col>
              <Col xs={12}>
                <br />
                <div>
                  <Button variant="primary" size="lg" type="submit">
                    Guardar
                  </Button>
                </div>
              </Col>
            </Row>
          </Container>
        </Col>
      </Row>
    </Form>
  );
};

export default FormAddProperty;
