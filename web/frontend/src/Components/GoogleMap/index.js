import React, { Component } from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import { getOperation, getPropertyType } from "../CardProperty";
import { Button } from "react-bootstrap";

const API_MAPS = process.env.REACT_APP_API_KEY_GOOGLE_MAPS;

const containerStyle = {
  position: "relative",
  width: "100%",
  height: "100%",
};

export class MapContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: null,
      longitude: null,
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      selectedMap: {},
    };
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.setState({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      });
    });
  }

  onMarkerClick = (props, marker, e) => {
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true,
    });
  };

  onMapClicked = (mapProps, map, clickEvent) => {
    const { onMoveMarker, createMarker } = this.props
    if(!createMarker){
      return
    }
    this.setState({
      selectedMap: {
        lat: clickEvent.latLng.lat(),
        lng: clickEvent.latLng.lng(),
      },
    });
    if(onMoveMarker && typeof onMoveMarker == "function"){
      const { selectedMap} = this.state
      onMoveMarker(selectedMap)
    }
  };

  onMoveMarkerM = (mapProps, map, clickEvent) => {
    const { onMoveMarker } = this.props
    this.setState({
      selectedMap: {
        lat: map.position.lat(),
        lng: map.position.lng(),
      },
    });
    if(onMoveMarker && typeof onMoveMarker == "function"){
      const { selectedMap} = this.state
      onMoveMarker(selectedMap)
    }
  };

  redirectPage = () => {
    window.location = `/properties/${this.state.selectedPlace.data.property_type}`;
  };

  render() {
    const {
      properties,
      propertyEdit,
      initialCenter: defInitialCenter,
      zoom = 10,
    } = this.props;
    const { latitude, longitude, selectedMap } = this.state;

    //var bounds = new this.props.google.maps.LatLngBounds();

    const propsMap = {
      zoom: zoom,
    }
    if(defInitialCenter && Object.keys(defInitialCenter).length > 0){
      propsMap.initialCenter =defInitialCenter
    }
    

    return (
      <div style={{ height: "400px" }}>
        <div style={containerStyle}>
          <Map
            google={this.props.google}
            containerStyle={containerStyle}
            style={{ with: "100%", height: "100%" }}
            {...propsMap}
            // bounds={bounds}
            onClick={this.onMapClicked}
          >
            {properties &&
              Array.isArray(properties) &&
              properties.length > 0 &&
              properties.map((property) => (
                <Marker
                  key={`mark_${property.id}`}
                  title={property.name}
                  name={property.name}
                  position={{ lat: property.latitude, lng: property.longitude }}
                  data={property}
                  onClick={this.onMarkerClick}
                />
              ))}
            {propertyEdit && Object.keys(propertyEdit).length > 0 && (
              <Marker
                position={{ lat: propertyEdit.lat, lng: propertyEdit.lng }}
                draggable
                onDragend={this.onMoveMarkerM}
              />
            )}
            {selectedMap && selectedMap !== null && (
              <Marker
                position={selectedMap}
                draggable
                onDragend={this.onMoveMarkerM}
              />
            )}

            {/* <Marker /> */}

            <InfoWindow
              marker={this.state.activeMarker}
              visible={this.state.showingInfoWindow}
            >
              {Object.keys(this.state.selectedPlace).length > 0 && (
                <div>
                  <h2>{this.state.selectedPlace.name}</h2>
                  <div>
                    {getOperation(this.state.selectedPlace.data.operation)} de{" "}
                    {getPropertyType(
                      this.state.selectedPlace.data.property_type
                    )}
                    <br />
                    <br />
                    <div>
                      <Button variant="primary" onClick={this.redirectPage}>
                        Ver detalle
                      </Button>
                    </div>
                  </div>
                </div>
              )}
            </InfoWindow>
          </Map>
        </div>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: API_MAPS,
})(MapContainer);
