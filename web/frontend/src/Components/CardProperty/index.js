import React from "react";
import { Col, Row, Container, Card, Carousel } from "react-bootstrap";
import { RiHotelBedLine } from "react-icons/ri";
import { FaShower } from "react-icons/fa";
import { BiArea } from "react-icons/bi";
import { Link } from "react-router-dom";
import { AiFillEye, AiOutlineDelete, AiFillEdit } from "react-icons/ai";

export const getOperation = (value) => {
  if (value === "SALE") {
    return "Venta";
  }
  if (value === "RENT") {
    return "Renta";
  }

  if (value === "TRANSFER") {
    return "Traspaso";
  }
  return "";
};

export const getPropertyType = (value) => {
  // 'HOUSE', 'APARTAMENT', 'TERRAIN', 'OFFICE', 'LOCAL'
  if (value === "HOUSE") {
    return "Casa";
  }
  if (value === "APARTAMENT") {
    return "Departamento";
  }

  if (value === "TERRAIN") {
    return "Terreno";
  }

  if (value === "OFFICE") {
    return "Oficina";
  }
  if (value === "LOCAL") {
    return "Local";
  }
  return "";
};

const API_URL_FILE = process.env.REACT_APP_IMAGE_URL;

const CardProperty = ({ property, isLoggin, onDeleteProperty }) => {
  const { gallery = [] } = property;

  const operation = getOperation(property.operation);
  const propertyType = getPropertyType(property.property_type);

  return (
    <Card className="card-property">
      <Card.Body className="card-property-body">
        <div className="property-carrusel">
          <Carousel
            variant="dark"
            indicators={false}
            interval={null}
            nextIcon={
              <span
                aria-hidden="true"
                className="carousel-control-next-icon"
                style={{ backgroundColor: "#000000", borderRadius: "50%" }}
              />
            }
            prevIcon={
              <span
                aria-hidden="true"
                className="carousel-control-prev-icon"
                style={{ backgroundColor: "#000000", borderRadius: "50%" }}
              />
            }
          >
            {gallery && Array.isArray(gallery) && gallery.length > 0 ? (
              gallery.map((image) => (
                <Carousel.Item key={`img_${image.id}`}>
                  <img
                    className="d-block w-100"
                    src={`${API_URL_FILE}/storage/${image.path}`}
                    alt="First slide"
                  />
                </Carousel.Item>
              ))
            ) : (
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQizXeIRipuuL5wSWWUdLZM_vmcRaoRx6OoZXZ5aHQ4x4IILrHPWPrb6LKuQcO6p-XJz5k&usqp=CAU"
                  alt="First slide"
                />
              </Carousel.Item>
            )}
          </Carousel>
        </div>
        <div className="amenity-section">
          <Container>
            <Row>
              <Col xs={4}>
                <RiHotelBedLine size="1.5em" color="#bbb" /> {property.bedrooms}
              </Col>
              <Col xs={4}>
                <FaShower size="1.2em" color="#bbb" /> {property.num_bathrooms}
              </Col>
              <Col xs={4}>
                <BiArea size="1.4em" color="#bbb" /> {property.m2_construction} m2
              </Col>
            </Row>
          </Container>
        </div>
        <div className="property-info">
          <div className="title">{property.name}</div>
          <div className="price">
            <span>$ {property.price} MN</span>
          </div>
          <p
            className={`address-property ${
              property.operation === "SALE" ? "sale" : "rent"
            }`}
          >
            {operation} de {propertyType} <br />
            en Colonia {property.neighborhood} {property.city}, CP {property.cp},{" "}
            {property.state}
          </p>
        </div>
        <br />
        <div className="property-actions">
          <Container>
            <Row className="justify-content-center">
              <Col xs={4}>
                <Link to={`/propiedad/${property.id}`}>
                  <button>
                    <AiFillEye size="1.8em" />
                  </button>
                </Link>
              </Col>
              {isLoggin && (
                <Col xs={4}>
                  <Link to={`/editar-propiedad/${property.id}`}>
                    <button>
                      <AiFillEdit size="1.8em" />
                    </button>
                  </Link>
                </Col>
              )}
              {isLoggin && (
                <Col xs={4}>
                  <button onClick={() => onDeleteProperty(property.id)}>
                    <AiOutlineDelete size="1.8em" />
                  </button>
                </Col>
              )}
            </Row>
          </Container>
        </div>
      </Card.Body>
    </Card>
  );
};

export default CardProperty;
