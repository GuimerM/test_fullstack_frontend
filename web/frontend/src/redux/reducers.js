import {
  GET_ALL_PROPERTIES,
  GET_PROPERTY,
  CREATE_PROPERTY,
  UPDATE_PROPERTY,
  DELETE_PROPERTY,
  GET_ALL_AMENITIES,
} from "./actions";

export const propertyReducer = (state = {}, action) => {
  if (action.type === GET_ALL_PROPERTIES) {
    return {
      ...state,
      properties: action.properties,
    };
  }

  if (action.type === GET_PROPERTY) {
    return {
      ...state,
      property: action.property,
    };
  }

  return state
};

export const amenityReducer = (state = {}, action) => {
  if (action.type === GET_ALL_AMENITIES) {
    return {
      ...state,
      amenities: action.amenities,
    };
  }

  return state
};
