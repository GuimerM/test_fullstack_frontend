import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { propertyReducer, amenityReducer } from "./reducers";

export default createStore(
  combineReducers({ propertyReducer, amenityReducer }),
  composeWithDevTools(applyMiddleware(thunk))
);
