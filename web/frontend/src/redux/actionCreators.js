import Axios from "axios";
import {
  GET_ALL_PROPERTIES,
  GET_PROPERTY,
  CREATE_PROPERTY,
  UPDATE_PROPERTY,
  DELETE_PROPERTY,
  GET_ALL_AMENITIES,
} from "./actions";

const API_URL = process.env.REACT_APP_API_URL
const token = localStorage.getItem('token')

export const getAllProperties = () => dispatch => {
    Axios.get(`${API_URL}/properties`).then(resp => {
        return dispatch({
            type: GET_ALL_PROPERTIES,
            properties: resp.data
        })
    })
}

export const getProperty = id => dispatch => {
    Axios.get(`${API_URL}/properties/${id}`).then( resp => {
        return dispatch({
            type: GET_PROPERTY,
            property: resp.data
        })
    })
}


export const getAllAmenities = id => dispatch => {
    Axios.get(`${API_URL}/amenities`).then( resp => {
        return dispatch({
            type: GET_ALL_AMENITIES,
            amenities: resp.data
        })
    })
}