import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/Home";
import PropertyPage from "./Pages/Property";
import RegisterPage from "./Pages/Register";
import LoginPage from "./Pages/Login";
import CreatePropertyPage from './Pages/CreateProperty'
import EditPropertyPage from "./Pages/EditProperty";
import Page404 from "./Pages/Page404";
import Protected from "./Routes/Protected";
import Public from "./Routes/Public";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<HomePage/>} path="/" />
        <Route element={<Protected />}>
          <Route element={<PropertyPage/>} path="/propiedad/:id" />
          <Route element={<CreatePropertyPage/>} path="/crear-propiedad" />
          <Route element={<EditPropertyPage/>} path="/editar-propiedad/:id" />
        </Route>
        <Route element={<Public/>}>
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/login" element={<LoginPage />} />
        </Route>
        <Route path="*" element={<Page404 />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
