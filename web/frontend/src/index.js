import React from 'react';
import ReactDOM from 'react-dom';
import 'react-flexbox-grid/dist/react-flexbox-grid.css'
import './styles/styles.scss';
import App from './App';
import { Provider } from 'react-redux'
import store from './redux/store'

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
